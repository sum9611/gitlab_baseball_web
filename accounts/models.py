# from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager as AbstractUserManager
from ckeditor_uploader.fields import RichTextUploadingField


class User(AbstractUser):
    password = models.CharField(max_length=256)

class UserManager(AbstractUserManager):
    pass

