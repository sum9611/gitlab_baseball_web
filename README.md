```
05/15 작업내용 

1. 지난번 말한 선수하나의 정보를 각각의 dict타입에 저장하여 전체 정보를 list에 담아두기 완료 
url = http://127.0.0.1:8000/baseballs/index2 에서 확인가능 
테이블 컬럼에 들어갈 부분은 주석처리 해놨음 하나씩 박아 넣으면 될듯 


2. DataFrame에 to_html모듈을 사용하여 테이블 형태 구성 
url = http://127.0.0.1:8000/baseballs/
간편하게 테이블 형태로 구현했으나 css 적용이 가능할지는 모르겠음 현재는 중앙정렬만 해둔 상태 

## 현재 데이터는 23년도 14주차만 가져오게 되어있음 만약 전체 데이터를 가지고 오고싶다면 
## views.py 34번 줄 주석 해제하고 사용하면 됨 위에 "sql = " 주석처리해야함 

3. 원래 DB에 있던 테이블들 baseballs/models.py에 전부 선언해두었음 
url = http://127.0.0.1:8000/baseballs/index3
명령어 "python manage.py inspectdb > 'baseballs/models.py' --database 'default'" 사용 
이제 기존에 있던 테이블들이 model에 선언되어서 쿼리셋형태로 가는것같음 
확인 해보면될듯 ~ 

``` 


 
 

 

# 가상환경 생성 
python -m venv venv

# 가상환경 접속 
## 디렉토리 이동 필요
# windows 환경
source venv/Scripts/activate

# ubuntu 환경
source venv/bin/activate

# 모듈설치 
pip install -r requirements.txt

## mysqlclient 에러발생시 
# sudo apt-get install libmysqlclient-dev 먼저 진행 

# 스크린 환경 접속
screen -R 103506.server

# DB 설정 동기화 
python manage.py makemigrations
python manage.py migrate

# 서버 실행 
python3 manage.py runserver 0:8000




## 로고만들기 
https://www.canva.com/


## Jira 연동 관련 

1. jira에 gitlab 앱 설치 

2. gitlab project -> settings -> intergrations -> jira 선택

3. "Web URL" 입력 ex)https://baseball-web.atlassian.net

4. "Email or username"에 jira에서 사용중인 이메일 입력 ex)sum9611@khu.ac.kr 

5. "New API token or password" 입력  --> jira에서 api 토큰을 생성하는 과정이 필요함

    1. jira 우측 계정을 누르고 계정 관리 클릭

    2. 상단 보안 클릭 -> API 토큰 만들기 및 관리 클릭 

    3. API 토큰 만들기 클릭 후 label 입력 

    4. 토큰 복사해서 "New API token or password" 입력

6. 하단 "Test settings" 버튼 클릭 후 connection 확인 후 "Save changes" 클릭 --> 연동 완료 

## Jira 연동 확인 

1. Repository 수정 

2. git commit 할때 메세지에 {Jira 프로젝트 키 값}-{숫자} 입력 후 comment 입력 ex)"BW-1 {comment}"
    - Jira 프로젝트 키 값은 프로젝트 설정란에서 확인가능 

3. git push 후 gitlab에서 comment를 클릭하면 Jira 링크로 연결 확인 




