from django import template

register = template.Library()

@register.filter
def highlight(value, query):
    if isinstance(value, str):
        return value.replace(query, f'<span class="highlight">{query}</span>')
    return value