from django import forms
from .models import Post, PostComment
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class CustomPost(forms.ModelForm):
    title = forms.CharField(
        label=False,
        widget=forms.TextInput(
            attrs={
                'placeholder': '제목',
                'class': 'my-2 fs-5 p-2 focus:outline-[#A2E04E] outline-offset-2 rounded',
                'style': 'width: 100%;',
            }
        )
    )
    
    content = forms.CharField(
        widget=CKEditorUploadingWidget(
            attrs={
                'placeholder': '내용',
            }
        ),
        label=''
    )

    category = forms.ChoiceField(
        choices=[('공지', '공지'), ('문의', '문의'), ('타자', '타자'), ('투수', '투수'), ('자유', '자유')],
        widget=forms.Select(
            attrs={
                'class': 'form-control'
            }
        )
    )

    class Meta:
        model = Post
        fields = ['title', 'category', 'content',]
        widgets = {
            'content': CKEditorUploadingWidget(),
        }


class CustomPostComment(forms.ModelForm):
    content = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'placeholder': '내용을 입력해 주세요.',
                'class': 'comment-content',
                'id': 'comment-content',
                'style': 'width: 100%; height: 100%; resize: none;',
                'aria-describedby': 'comment-username-description',
            }
        ),
        label=''
    )

    comment_pw = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Password',
                'class': 'comment-pw',
                'id': 'comment-pw',
                'style': 'width: 100%; height: 100%; resize: none;',
                'autocomplete': 'new-password',
            }
        ),
        required=False,
    )
    
    class Meta:
        model = PostComment
        fields = ['content', 'comment_pw']