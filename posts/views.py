from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.hashers import make_password, check_password
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from .models import Post, PostComment
from .forms import CustomPost, CustomPostComment
from django import forms
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from .utils import get_client_ip

from bs4 import BeautifulSoup


# Create your views here.
User = get_user_model()

def get_first_image_from_content(content):
    soup = BeautifulSoup(content, 'html.parser')
    img_tag = soup.find('img')
    if img_tag:
        return img_tag['src']
    else:
        return None

def index(request):
    posts = Post.objects.all().order_by('-id')

    # 페이지네이션
    page = request.GET.get('page', '1')
    per_page = 10
    paginator = Paginator(posts, per_page)
    page_obj = paginator.get_page(page)

    context = {
        'posts': posts,
        'page_obj': page_obj,
    }
    return render(request, 'posts/index.html', context)


def index2(request):
    posts = Post.objects.all().order_by('-id')

    # 페이지네이션
    page = request.GET.get('page', '1')
    per_page = 10
    paginator = Paginator(posts, per_page)
    page_obj = paginator.get_page(page)

    context = {
        'posts': posts,
        'page_obj': page_obj,
    }
    return render(request, 'posts/notice.html', context)


def index3(request):
    posts = Post.objects.all().order_by('-id')

    # 페이지네이션
    page = request.GET.get('page', '1')
    per_page = 10
    paginator = Paginator(posts, per_page)
    page_obj = paginator.get_page(page)

    context = {
        'posts': posts,
        'page_obj': page_obj,
    }
    return render(request, 'posts/customercenter.html', context)


def search(request):
    query = request.GET.get('q')
    search_option = request.GET.get('search_option')

    if query and search_option:
        if search_option == 'title':
            filtered_posts = Post.objects.filter(title__contains=query).order_by('-id')
        elif search_option == 'author':
            filtered_posts = Post.objects.filter(user__username__icontains=query).order_by('-id')
        elif search_option == 'content':
            filtered_posts = Post.objects.filter(content__contains=query).order_by('-id')
        elif search_option == 'title_content':
            filtered_posts = Post.objects.filter(Q(title__contains=query) | Q(content__contains=query)).order_by('-id')
    else:
        filtered_posts = Post.objects.order_by('-id')

    # 페이지네이션
    page = request.GET.get('page', '1')
    per_page = 10
    paginator = Paginator(filtered_posts, per_page)
    page_obj = paginator.get_page(page)

    context = {
        'page_obj': page_obj,
        'query': query,
        'search_option': search_option,
    }
    return render(request, 'posts/index.html', context)


def detail(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    postcomment_form = CustomPostComment()
    postcomments = post.postcomment_set.order_by('-id')

    # 포스트의 뷰 카운트 증가시키기
    post.view_count += 1
    post.save()
    
    prev_post = Post.objects.filter(pk__gt=post_pk).order_by('pk').first()
    next_post = Post.objects.filter(pk__lt=post_pk).order_by('-pk').first()

    # 페이지네이션
    page = request.GET.get('page', '1')
    per_page = 10
    paginator = Paginator(postcomments, per_page)
    page_obj = paginator.get_page(page)

    context = {
        'post': post,
        'postcomment_form': postcomment_form,
        'postcomments': postcomments,
        'prev_post': prev_post,
        'next_post': next_post,
        'page_obj': page_obj,
    }
    return render(request, 'posts/detail.html', context)


@login_required
def create(request):
    if request.method == 'POST':
        form = CustomPost(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            return redirect('posts:index')
    else:
        form = CustomPost()

    context = {
        'form': form,
    }
    return render(request, 'posts/create.html', context)


@login_required
def update(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    if request.user != post.user:
        return redirect('posts:detail', post.pk)
    
    if request.method == 'POST':
        form = CustomPost(request.POST, request.FILES, instance=post)
        if form.is_valid():
            post.save()
            return redirect('posts:detail', post.pk)
    else:
        form = CustomPost(instance=post)

    context = {
        'post': post,
        'form': form,
    }

    return render(request, 'posts/update.html', context)


@login_required
def delete(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    if request.user == post.user:
        post.delete()
    return redirect('posts:index')


@login_required
def likes(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
        
    if post.like_users.filter(pk=request.user.pk).exists():
        post.like_users.remove(request.user)
        is_liked = False
    else:
        post.like_users.add(request.user)
        is_liked = True
    
    
    context = {
        'is_liked':is_liked,
        'like_count':post.like_users.count()
    }
    return JsonResponse(context)


def comment_create(request, post_pk):
    post = Post.objects.get(pk=post_pk)
    postcomment_form = CustomPostComment(request.POST)

    if postcomment_form.is_valid():
        postcomment = postcomment_form.save(commit=False)
        postcomment.post = post

        # 미인증 사용자일 경우 댓글 작성자 None 처리
        if request.user.is_authenticated:
            postcomment.user = request.user
            comment_pw = request.user.password
            postcomment.comment_pw = comment_pw
            
        else:
            postcomment.user = None
            postcomment.anonymous_ip = get_client_ip(request)

            # 비밀번호 확인
            comment_pw = request.POST.get('comment_pw')
            if not comment_pw and not request.user.is_authenticated:
                return HttpResponse('비밀번호를 입력해주세요')
        
            postcomment.comment_pw = make_password(comment_pw)

        postcomment.save()
        return redirect('posts:detail', post_pk)
    else:
        postcomment_form = CustomPostComment()
        
    context = {
        'post': post,
        'postcomment_form': postcomment_form,
    }

    return render(request, 'posts/detail.html', context)


def comment_update(request, post_pk, comment_pk):
    postcomment = PostComment.objects.get(pk=comment_pk)

    if request.method == 'POST':
        form = CustomPostComment(request.POST, instance=postcomment)
        if form.is_valid():
            form.save()
            return redirect('posts:detail', post_pk)
    else:
        form = CustomPostComment(instance=postcomment)

    context = {
        'form': form,
        'post_pk': post_pk,
        'comment_pk': comment_pk,
    }
    return render(request, 'posts/comment_update.html', context)


def comment_delete(request, post_pk, comment_pk):
    postcomment = PostComment.objects.get(pk=comment_pk)

    if request.method == 'POST':
        comment_pw = request.POST.get('comment_pw')

        if not comment_pw:
            return JsonResponse({'status': 'error', 'message': '비밀번호를 입력해주세요.'})
        
        if postcomment.user:
            if request.user.is_authenticated and postcomment.user == request.user:
                # 로그인한 사용자의 비밀번호를 확인하여 인증
                user = authenticate(request, username=request.user.username, password=comment_pw)
                if user is None:
                    return JsonResponse({'status': 'error', 'message': '비밀번호가 일치하지 않습니다.'})
                else:
                    # 비밀번호가 일치하면 댓글 삭제
                    postcomment.delete()
                    post_comments = PostComment.objects.filter(post=post_pk).count()
                    return redirect('posts:detail', post_pk)
            else:
                # 작성자가 아닌 경우 권한 오류 메시지 반환
                return JsonResponse({'status': 'error', 'message': '권한이 없습니다.'})
        else:
            # 익명 댓글인 경우 비밀번호를 확인하여 삭제
            if check_password(comment_pw, postcomment.comment_pw):
                postcomment.delete()
                post_comments = PostComment.objects.filter(post=post_pk).count()
                return redirect('posts:detail', post_pk)
            else:
                return JsonResponse({'status': 'error', 'message': '비밀번호가 일치하지 않습니다.'})
                
    else:
        return redirect('posts:detail', post_pk)


