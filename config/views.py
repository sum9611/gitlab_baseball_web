from django.shortcuts import render
import yaml
import pandas as pd
import pymysql


from django.views.decorators.http import require_GET
from django.http import JsonResponse
from django.http import HttpResponse



with open('./yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']


def get_data():
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select * from team_rank
    '''
    cur.execute(sql)
    result = cur.fetchall()
    team_rank = pd.DataFrame(result)
    return team_rank



def index(request):
    team_rank = get_data()
    context = {'dataframe': team_rank}


    return render(request, 'default.html',context)