// 모달 열기
function openModal(message) {
  document.getElementById("error-modal").style.display = "block";
}

// 모달 닫기
function closeModal() {
  document.getElementById("error-modal").style.display = "none";
}


// post like
const forms = document.querySelectorAll(".like-form");
const postCsrftokenLike = document.querySelector("[name=csrfmiddlewaretoken]").value;

forms.forEach((form) => {
    form.addEventListener("submit", function(event) {
      event.preventDefault();
      const postId = event.target.dataset.postId;

      axios({
        method: "post",
        url: `/posts/${postId}/likes/`,
        headers: {"X-CSRFToken" : postCsrftokenLike},
      }).then((response) => {
          const isLiked = response.data.is_liked;
          const likeBtn = form.querySelector("#like-btn");
          if (isLiked) {
            likeBtn.className = "like-btn bi bi-hand-thumbs-up-fill thumb--color like--hover"
          } else {
            likeBtn.className = "like-btn bi bi-hand-thumbs-up like--hover"
          }
          const likeCountTag = form.querySelector(".like-count")
          const likeCountData = response.data.like_count
          likeCountTag.textContent = likeCountData
      })
      .catch((error) => {
          console.log(error.response)
      });
    });
});

// 댓글 수정

jQuery(document).ready(function() {
  jQuery('.edit-comment-button2').click(function() {
      var commentId = jQuery(this).data('comment-id');
      jQuery('#comment-' + commentId + ' .comment-content').toggle();
      jQuery('#comment-edit-form-' + commentId).toggle();
      jQuery('#comment-etc-' + commentId).toggle();
  });

  jQuery('.edit-comment-form w-full d-flexpy-2').submit(function(event) {
      event.preventDefault();
      var commentId = jQuery(this).data('comment-id');
      var content = jQuery(this).find('input[name=content]').val();
      var form = jQuery(this);

      $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: form.serialize(),
        success: function(response) {
          // Update the comment content
          $('#comment-' + commentId + ' .comment-content').text(content);
          // Show the edit button again
          $('.edit-comment-button[data-comment-id=' + commentId + ']').toggle();
          // Hide the edit form
          $('#comment-edit-form-' + commentId).toggle();
          $('#comment-' + commentId + ' .comment-content').toggle();
        },
        error: function(xhr, status, error) {
          console.error(xhr.responseText);
        }
      });
    });
})

// "취소" 버튼 클릭 시
jQuery('.cancel-comment-button').click(function() {
    var commentId = jQuery(this).data('comment-id');
    jQuery('#comment-' + commentId + ' .comment-content').toggle();
    jQuery('#comment-edit-form-' + commentId).toggle();
    jQuery('#comment-etc-' + commentId).toggle();
});

