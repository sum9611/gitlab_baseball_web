from django.urls import path

from . import views


app_name = 'baseballs'
urlpatterns = [
    path('index', views.set_index, name='set_index'),
    path('change_index1', views.change_index1, name='change_index1'),
    path('remove', views.remove, name='remove'),
    path('get_index', views.get_index, name='get_index'),
    path('autocomplete', views.autocomplete, name='autocomplete'),
    
    # 투수쪽
    path('index2', views.set_index2, name = 'set_index2'),
    path('change_index2', views.change_index2, name='change_index2'),
    path('get_index2', views.get_index2, name='get_index2'),
    path('autocomplete2', views.autocomplete2, name='autocomplete2'),

    # rank 
    path('b_rank', views.b_rank, name = 'b_rank'),
    path('p_rank', views.p_rank, name = 'p_rank'),

    # rank 비교 
    path('b_com', views.b_com, name = 'b_com'),
    path('p_com', views.p_com, name = 'p_com'),

]