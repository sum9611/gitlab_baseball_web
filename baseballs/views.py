from django.shortcuts import render, redirect
# from .models import WeeklyBattingInfo
import yaml
import pandas as pd
import pymysql


from django.views.decorators.http import require_GET
from django.http import JsonResponse
from django.http import HttpResponse

# db정보 가져오기
with open('./yamls/sql_info.yaml') as f:

    info = yaml.load(f, Loader=yaml.FullLoader)

host = info['MARIADB']['IP']
user = info['MARIADB']['USER']
passwd=info['MARIADB']['PASSWD']
db = info['MARIADB']['DB']
port = info['MARIADB']['PORT']


##test 


def get_autodata():
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select concat(A.player_name,' ',A.team,' ',B.player_birth, ' ' ,A.player_ID) as data from (
        select player_ID ,player_name, GROUP_CONCAT(DISTINCT TEAM) as team from batting_info bi
        group by player_ID ,player_name ) A join player_birth B on A.player_ID = B.player_ID  
    '''
    cur.execute(sql)
    result = cur.fetchall()
    auto_data = pd.DataFrame(result)
    # print(auto_data)
    return auto_data


def get_autodata2():
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select concat(A.player_name,' ',A.team,' ',B.player_birth, ' ' ,A.player_ID) as data from (
        select player_ID ,player_name, GROUP_CONCAT(DISTINCT TEAM) as team from pitching_info bi
        group by player_ID ,player_name ) A join player_birth B on A.player_ID = B.player_ID 
    '''
    cur.execute(sql)
    result = cur.fetchall()
    auto_data = pd.DataFrame(result)
    # print(auto_data)
    return auto_data


def get_data(p_name,p_id):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select week,sum(H) as `H` from weekly_batting_info
            where 1=1
            and player_name = '{p_name}'
            and player_ID = '{p_id}'
            group by week
    '''
    cur.execute(sql)
    result = cur.fetchall()
    batting = pd.DataFrame(result)
    print(batting)
    conn.close()
    return batting


def change_data(p_name,p_id,idx):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select week,sum({idx}) as `H` from weekly_batting_info
            where 1=1
            and player_name = '{p_name}'
            and player_ID = '{p_id}'
            group by week
    '''
    cur.execute(sql)
    result = cur.fetchall()
    batting = pd.DataFrame(result)
    print(batting)
    conn.close()
    return batting

def get_data2(p_name,p_id):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select week, sum(ER) / sum(IP) * 9 as `ERA`  from weekly_pitching_info wpi 
            where 1=1
            and player_name = '{p_name}'
            and player_ID ='{p_id}'
            group by week
    '''
    cur.execute(sql)
    result = cur.fetchall()
    pitching = pd.DataFrame(result)
    print(pitching)
    conn.close()
    return pitching

def change_data2(p_name,p_id,idx):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select week,sum({idx}) as `ERA` from weekly_pitching_info
            where 1=1
            and player_name = '{p_name}'
            and player_ID = '{p_id}'
            group by week
    '''
    print(sql)
    cur.execute(sql)
    result = cur.fetchall()
    pitching = pd.DataFrame(result)
    print(pitching)
    conn.close()
    return pitching



def get_rank_data(player_type):
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select * from rank_info 
    where 1=1
    and player_type = '{player_type}'
    order by total_point DESC '''
    cur.execute(sql)
    result = cur.fetchall()
    rank = pd.DataFrame(result)
    return rank

def get_week():
    conn = pymysql.connect(host = host, user = user, passwd=passwd, db = db, charset='utf8', port = port, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    sql = f'''select week  from weekly_batting_info
            group by week 
    '''
    cur.execute(sql)
    result = cur.fetchall()
    week = pd.DataFrame(result)
    conn.close()
    return week



week = get_week()
week_list = week['week'].to_list()
auto_data = get_autodata()
auto_data2 = get_autodata2()

def set_index(request):

    request.session['p_name'] = ['강백호']
    request.session['p_id'] = ['68050']
    idx = '안타'
    request.session['idx'] = idx
    
    
    p_name = request.session.get('p_name')
    p_id = request.session.get('p_id')
    hit_data = get_data('강백호','68050')
    hit_data = pd.merge(hit_data[['week','H']], week, on = 'week', how='right')
    hit_data_list = hit_data['H'].fillna(0).astype(int).to_list()
    request.session['hit_data'] = [hit_data_list]
    hit = request.session.get('hit_data')

    context = {
                'idx' : idx,
                'p_name' : p_name,
                'p_id' : p_id,
                'zip_info' : zip(p_name,p_id),
                'week' : week_list,
                'result' : zip(p_name,hit,p_id),
            }

    return render(request, 'baseballs/index.html',context)

def get_index(request):
    
    # 기존 리스트 
    b_name = request.session.get('p_name', [])
    b_hit = request.session.get('hit_data', [])
    b_id = request.session.get('p_id',[])

    idx = request.session.get('idx')
    
    if idx == '안타':
        col = 'H'
    elif idx == '타점':
        col = 'RBI'
    elif idx == '홈런':
        col = 'HR'
    
    # 신규 선수 이름 
    re_val = request.POST['add_name'].split(' ')


    add_name = re_val[0]

    ############################################################
    ## 자동완성을 사용하지 않을 시 화면에 경고창 표시가 필요함 ! ##
    ############################################################
    add_id = re_val[5]
    hit_data = change_data(add_name,add_id,col)

    # 1. 조회된선수가 없는 경우 -> 기존 값 반환 
    if len(hit_data) == 0:
        context = {
                'idx' : idx,
                'p_name' : b_name,
                'p_id' : b_id,
                'zip_info' : zip(b_name,b_id),
                'week' : week_list,
                'result' : zip(b_name,b_hit,b_id),
            }
        return render(request, 'baseballs/index.html',context)

    # 2. 조회한선수가 이미 있는 경우 -> 기존 값 반환 
    elif add_id in request.session['p_id']:
        context = {
                'idx' : idx,
                'p_name' : b_name,
                'p_id' : b_id,
                'zip_info' : zip(b_name,b_id),
                'week' : week_list,
                'result' : zip(b_name,b_hit,b_id),
            }
        return render(request, 'baseballs/index.html',context)
    
    # 3. 조회선수 추가
    else:
        # 신규 선수 h list 
        hit_data = pd.merge(hit_data[['week','H']], week, on = 'week', how='right')
        hit_data_list = hit_data['H'].fillna(0).astype(int).to_list()

        # session 추가
        b_name.append(add_name)
        b_hit.append(hit_data_list)
        b_id.append(add_id)
        request.session['p_name'] = b_name
        request.session['hit_data'] = b_hit
        request.session['p_id'] = b_id

        p_name = request.session.get('p_name')
        p_id = request.session.get('p_id')
        hit = request.session.get('hit_data')


        context = {
                    'idx' : idx,
                    'p_name' : p_name,
                    'p_id' : p_id,
                    'zip_info' : zip(p_name,p_id),
                    'week' : week_list,
                    'result' : zip(p_name,hit,p_id),
                }
        return render(request, 'baseballs/index.html',context)


def change_index1(request):

    idx = request.POST['selected_value']
    
    request.session['idx'] = idx
    
    if idx == '안타':
        col = 'H'
    elif idx == '타점':
        col = 'RBI'
    elif idx == '홈런':
        col = 'HR'
    
    p_name = request.session.get('p_name')
    p_id = request.session.get('p_id')
    
    
    hit_list = []
    for pname,pid in zip(p_name,p_id):
        
        hit_data = change_data(pname,pid,col)
        hit_data = pd.merge(hit_data[['week','H']], week, on = 'week', how='right')
        hit_list.append(hit_data['H'].fillna(0).astype(int).to_list())
        
    request.session['hit_data'] = hit_list        

    context = {
                'idx' : idx,
                'p_name' : p_name,
                'p_id' : p_id,
                'zip_info' : zip(p_name,p_id),
                'week' : week_list,
                'result' : zip(p_name,hit_list,p_id),
            }
    
    
    
    return render(request,'baseballs/index.html', context)



def remove(request):
    
    idxs = request.session.get('idx')
    
    #제거 대상
    if 'remove_name' in request.POST:
        remove_id = request.POST['remove_name']
        
        # 기존 리스트 불러오기
        b_name = request.session.get('p_name', [])
        b_hit = request.session.get('hit_data', [])
        b_id = request.session.get('p_id',[])


        # 제거 대상 idx 찾기 
        try:
            idx = b_id.index(remove_id)
        
        # 제거 페이지 새로고침시 에러 회피를 위한 부분
        except:
            p_name = request.session.get('p_name')
            hit = request.session.get('hit_data')
            p_id = request.session.get('p_id')

            context = {
                        'idx' : idxs,
                        'p_name' : p_name,
                        'week' : week_list,
                        'zip_info' : zip(p_name,p_id),
                        'result' : zip(p_name,hit,p_id),
                    }

            return render(request, 'baseballs/index.html',context)

        # idx제거하기 
        b_name.pop(idx)
        b_hit.pop(idx)
        b_id.pop(idx)

        # 제거 대상 제거 후 session 선언 
        request.session['p_name'] = b_name
        request.session['hit_data'] = b_hit
        request.session['p_id'] = b_id

        p_name = request.session.get('p_name')
        hit = request.session.get('hit_data')
        p_id = request.session.get('p_id')
        context = {
                        'idx' : idxs,
                        'p_name' : p_name,
                        'zip_info' : zip(p_name,p_id),
                        'week' : week_list,
                        'result' : zip(p_name,hit,p_id),
                    }

        return render(request, 'baseballs/index.html',context)
    
    else:
        remove_id = request.POST['remove_name2']
        
        # 기존 리스트 불러오기
        b_name = request.session.get('p_name2', [])
        b_pit = request.session.get('pit_data', [])
        b_id = request.session.get('p_id2',[])


        # 제거 대상 idx 찾기 
        try:
            idx = b_id.index(remove_id)
        
        # 제거 페이지 새로고침시 에러 회피를 위한 부분
        except:
            p_name = request.session.get('p_name2')
            pit = request.session.get('pit_data')
            p_id = request.session.get('p_id2')

            context = {
                        'idx' : idxs,
                        'p_name' : p_name,
                        'week' : week_list,
                        'zip_info' : zip(p_name,p_id),
                        'result' : zip(p_name,pit,p_id),
                    }

            return render(request, 'baseballs/index2.html',context)

        # idx제거하기 
        b_name.pop(idx)
        b_pit.pop(idx)
        b_id.pop(idx)

        # 제거 대상 제거 후 session 선언 
        request.session['p_name2'] = b_name
        request.session['pit_data'] = b_pit
        request.session['p_id2'] = b_id

        p_name = request.session.get('p_name2')
        pit = request.session.get('pit_data')
        p_id = request.session.get('p_id2')
        context = {
                        'idx' : idxs,
                        'p_name' : p_name,
                        'zip_info' : zip(p_name,p_id),
                        'week' : week_list,
                        'result' : zip(p_name,pit,p_id),
                    }

        return render(request, 'baseballs/index2.html',context)


## 투수쪽

def set_index2(request):

    request.session['p_name2'] = ['알칸타라']
    request.session['p_id2'] = ['69045']
    idx = '방어율'

    request.session['idx'] = idx

    p_name = request.session.get('p_name2')
    p_id = request.session.get('p_id2')
    pit_data = get_data2('알칸타라','69045')
    pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
    pit_data['ERA'] = pit_data['ERA'].apply(lambda x: "{:.2f}".format(x) if not pd.isnull(x) else '-1.0')
    pit_data_list = pit_data['ERA'].astype(float).to_list()
    request.session['pit_data'] = [pit_data_list]
    pit = request.session.get('pit_data')

    context = {
                'idx' : idx,
                'p_name' : p_name,
                'p_id' : p_id,
                'zip_info' : zip(p_name,p_id),
                'week' : week_list,
                'result' : zip(p_name,pit,p_id),
            }

    return render(request, 'baseballs/index2.html',context)


def get_index2(request):
    # 기존 리스트 
    b_name = request.session.get('p_name2', [])
    b_pit = request.session.get('pit_data', [])
    b_id = request.session.get('p_id2',[])

    idx = request.session.get('idx')


    if idx == '방어율':
        col = 'ERA'
    elif idx == '삼진':
        col = 'K'
    elif idx == '피안타':
        col = 'H'
    elif idx == '피홈런':
        col = 'HR'

    # 신규 선수 이름 
    re_val = request.POST['add_name'].split(' ')


    add_name = re_val[0]

    ############################################################
    ## 자동완성을 사용하지 않을 시 화면에 경고창 표시가 필요함 ! ##
    ############################################################
    add_id = re_val[5]

    if col == 'ERA':
        pit_data = get_data2(add_name,add_id)
                   
    else: 
        pit_data = change_data2(add_name,add_id,col)

    # 1. 조회된선수가 없는 경우 -> 기존 값 반환 
    if len(pit_data) == 0:
        context = {
                'idx' : idx,
                'p_name' : b_name,
                'p_id' : b_id,
                'zip_info' : zip(b_name,b_id),
                'week' : week_list,
                'result' : zip(b_name,b_pit,b_id),
            }
        return render(request, 'baseballs/index2.html',context)

    # 2. 조회한선수가 이미 있는 경우 -> 기존 값 반환 
    elif add_id in request.session['p_id2']:
        context = {
                'idx' : idx,
                'p_name' : b_name,
                'p_id' : b_id,
                'zip_info' : zip(b_name,b_id),
                'week' : week_list,
                'result' : zip(b_name,b_pit,b_id),
            }
        return render(request, 'baseballs/index2.html',context)
    
    # 3. 조회선수 추가
    else:
        # 신규 선수  
        if col == 'ERA':
            pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
            pit_data['ERA'] = pit_data['ERA'].apply(lambda x: "{:.2f}".format(x) if not pd.isnull(x) else '-1.0')
            pit_data_list = pit_data['ERA'].astype(float).to_list()

        else: 
            pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
            pit_data_list = pit_data['ERA'].fillna(0).astype(int).to_list()

        # session 추가
        b_name.append(add_name)
        b_pit.append(pit_data_list)
        b_id.append(add_id)
        request.session['p_name2'] = b_name
        request.session['pit_data'] = b_pit
        request.session['p_id2'] = b_id

        p_name = request.session.get('p_name2')
        p_id = request.session.get('p_id2')
        pit = request.session.get('pit_data')

        context = {
                    'idx' : idx,
                    'p_name' : p_name,
                    'p_id' : p_id,
                    'zip_info' : zip(p_name,p_id),
                    'week' : week_list,
                    'result' : zip(p_name,pit,p_id),
                }


        return render(request, 'baseballs/index2.html',context)


def change_index2(request):

    idx = request.POST['selected_value']
    
    request.session['idx'] = idx
    
    if idx == '방어율':
        col = 'ERA'
    elif idx == '삼진':
        col = 'K'
    elif idx == '피안타':
        col = 'H'
    elif idx == '피홈런':
        col = 'HR'
    
    p_name = request.session.get('p_name2')
    p_id = request.session.get('p_id2')
    
    
    pit_list = []
    ## 여기부터 

    for pname,pid in zip(p_name,p_id):
        

        if col == 'ERA':

            pit_data = get_data2(pname,pid)
            pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
            pit_data['ERA'] = pit_data['ERA'].apply(lambda x: "{:.2f}".format(x) if not pd.isnull(x) else '-1.0')
            pit_list.append(pit_data['ERA'].astype(float).to_list())
        
        else: 
            pit_data = change_data2(pname,pid,col)
            pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
            pit_list.append(pit_data['ERA'].fillna(0).astype(int).to_list())
        


    request.session['pit_data'] = pit_list        

    context = {
                'idx' : idx,
                'p_name' : p_name,
                'p_id' : p_id,
                'zip_info' : zip(p_name,p_id),
                'week' : week_list,
                'result' : zip(p_name,pit_list,p_id),
            }
    
    
    
    return render(request,'baseballs/index2.html', context)







def b_rank(request):
    data = get_rank_data('H')
    data['랭킹'] = data['total_point'].rank(ascending=False).astype(int)
    data = data[['랭킹', 'player_name','player_ID','total_point']]
    data = data.rename(columns={'player_name' : '선수이름', 'player_ID' : '고유선수번호', 'total_point':'점수'})
    
    data_table = data.to_dict(orient='records')

    context = {
                    
                    'datatable' : data_table
                }

    return render(request, 'baseballs/b_rank.html',context)


def p_rank(request):
    data = get_rank_data('P')
    data['랭킹'] = data['total_point'].rank(ascending=False).astype(int)
    data = data[['랭킹', 'player_name','player_ID','total_point']]
    data = data.rename(columns={'player_name' : '선수이름', 'player_ID' : '고유선수번호', 'total_point':'점수'})
    data_table = data.to_dict(orient='records')
    
    context = {
                    'datatable' : data_table
                }

    return render(request, 'baseballs/p_rank.html',context)




def b_com(request):

    idx = '안타'
    request.session['idx'] = idx
    selected_data = request.POST.get('selected_data')
    # 선택된 데이터를 활용하여 원하는 동작을 수행합니다.
    print(selected_data)
    selected_list = selected_data.split(',')

    name_list = []
    id_list = []
    hit_list = []

    for i in selected_list:
        print(i)
        data = i.split(' ')
        name_list.append(data[1])
        id_list.append(data[2])
        hit_data = get_data(data[1],data[2])
        hit_data = pd.merge(hit_data[['week','H']], week, on = 'week', how='right')
        hit_list.append(hit_data['H'].fillna(0).astype(int).to_list())


    request.session['p_name'] = name_list
    request.session['p_id'] = id_list
    request.session['hit_data'] = hit_list

    context = {
            'idx' : idx,
            'p_name' : name_list,
            'p_id' : id_list,
            'zip_info' : zip(name_list,id_list),
            'week' : week_list,
            'result' : zip(name_list,hit_list,id_list),
        }

    return render(request, 'baseballs/index.html',context)



def p_com(request):

    idx = '방어율'
    request.session['idx'] = idx

    selected_data = request.POST.get('selected_data')
    # 선택된 데이터를 활용하여 원하는 동작을 수행합니다.
    print(selected_data)
    selected_list = selected_data.split(',')

    name_list = []
    id_list = []
    pit_list = []

    for i in selected_list:
        print(i)
        data = i.split(' ')
        name_list.append(data[1])
        id_list.append(data[2])

        pit_data = get_data2(data[1],data[2])
        pit_data = pd.merge(pit_data[['week','ERA']], week, on = 'week', how='right')
        pit_data['ERA'] = pit_data['ERA'].apply(lambda x: "{:.2f}".format(x) if not pd.isnull(x) else '-1.0')

        pit_list.append(pit_data['ERA'].astype(float).to_list())


    request.session['p_name2'] = name_list
    request.session['p_id2'] = id_list
    request.session['pit_data'] = pit_list

    context = {
            'idx' : idx,
            'p_name' : name_list,
            'p_id' : id_list,
            'zip_info' : zip(name_list,id_list),
            'week' : week_list,
            'result' : zip(name_list,pit_list,id_list),
        }

    return render(request, 'baseballs/index2.html',context)






@require_GET
def autocomplete(request):
    query = request.GET.get('term', '')  
    data = auto_data['data'].to_list()

    results = [item for item in data if query in item]  
    
    return JsonResponse(results, safe=False)

@require_GET
def autocomplete2(request):
    query = request.GET.get('term', '')  
    data = auto_data2['data'].to_list()

    results = [item for item in data if query in item]  
    
    return JsonResponse(results, safe=False)


