# Generated by Django 3.2.18 on 2023-06-30 08:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('baseballs', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AccountsUser',
        ),
        migrations.DeleteModel(
            name='AccountsUserGroups',
        ),
        migrations.DeleteModel(
            name='AccountsUserUserPermissions',
        ),
        migrations.DeleteModel(
            name='AuthGroup',
        ),
        migrations.DeleteModel(
            name='AuthGroupPermissions',
        ),
        migrations.DeleteModel(
            name='AuthPermission',
        ),
        migrations.DeleteModel(
            name='DjangoAdminLog',
        ),
        migrations.DeleteModel(
            name='DjangoContentType',
        ),
        migrations.DeleteModel(
            name='DjangoMigrations',
        ),
        migrations.DeleteModel(
            name='DjangoSession',
        ),
        migrations.DeleteModel(
            name='Test',
        ),
    ]
